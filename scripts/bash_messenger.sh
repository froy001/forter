#!/bin/bash

LOGSTASH_URL="logstash"
LOGSTASH_PORT="8080"

while :
do
    if curl --output /dev/null --silent --head --fail "$LOGSTASH_URL:$LOGSTASH_PORT"; then
        curl -XPUT "$LOGSTASH_URL:$LOGSTASH_PORT" -d "$(hostname -i): $MESSAGE"
    else
        echo "waiting for logstash..."
    fi
    sleep 2
done
