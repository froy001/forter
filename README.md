## To run the solution on linux:

1. clone repo and cd forter
2. run docker-compose up
3. if using a linux machine: tail -f /var/lib/docker/volumes/forter_logstash/_data/app.log
4. if using a different os look in the docker documentation for the storage location of docker volumes:
    * tail -f \<volumes storage location\>/forter_logstash/_data/app.log

## To rum the solution on OSX:
To access persistent volumes created by Docker for Mac, you need to log in
to the hidden virtual machine docker runs on in an OSX system.

In order to accomplish this, you need to use a serial terminal on Mac. There's
a terminal application called "screen" that's going to help you.

1. "screen into" the Docker driver by executing a command: \`screen ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/tty\`
2. You should see a blank screen, just press Enter , and after a while, you should see a command line prompt:
docker-desktop:~#
3. Now you're inside Docker's VM and you can tail -f the log: 
    * tail -f /var/lib/docker/volumes/forter_logstash/_data/app.log

